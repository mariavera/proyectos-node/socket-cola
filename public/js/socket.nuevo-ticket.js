var socket = io();
var label = $('#lblNuevoTicket')
socket.on('connect', function () {
    console.log('Conectado al servidor');
});

//ESCUCHAR

socket.on('disconnect', function () {
    console.log('Perdimos conexion con el servidor');
})

socket.on('estadoActual', function(actual){
    label.text(actual.actual)
})

$('button').on('click', function () {
    socket.emit('siguienteTicket', null, function (siguienteTicket) {
        label.text(siguienteTicket)
    });

})